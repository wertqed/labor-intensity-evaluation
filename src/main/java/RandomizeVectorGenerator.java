import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class RandomizeVectorGenerator {

    public static void main(String[] args) throws IOException {
        RandomizeVectorGenerator.generateRandomizeVector(new File("code_complexity_reports.csv"), 5000);
    }

    public static void generateRandomizeVector(File file, int iterationsCnt) throws IOException {
        Scanner includeVectorScanner = new Scanner(file);
        FileWriter writer = new FileWriter("randomized_report.csv", false);
        writer.write("Jira Key," +
                "Jira Type," +
                "Jira Assignee," +
                "Story Points," +
                "Cyclomatic," +
                "CyclomaticDensity," +
                "ParamCount," +
                "Halstead Bugs," +
                "Halstead Difficulty," +
                "Halstead Effort," +
                "Halstead Length," +
                "Halstead Time," +
                "Halstead Vocabulary," +
                "Halstead Volume," +
                "Halstead Operators Total," +
                "Halstead Operators Distinct," +
                "Halstead Operands Total," +
                "Halstead Operands Distinct," +
                "Sloc Logical," +
                "Sloc Physical" +
                "\n");
        
        Random rand = new Random();
        int inputFileRowCnt = 0;
        while (includeVectorScanner.hasNextLine()) {
            includeVectorScanner.nextLine();
            inputFileRowCnt++;
        }
        for (int i = 0; i < iterationsCnt; i++) {
            int rowsToSum = rand.nextInt(7) +1;// Пусть будет 7
            String[] data = null;
            for (int j = 0; j < rowsToSum; j++) {
                int currRow = rand.nextInt(inputFileRowCnt) + 1;//Чтобы не напороться на первую строку
                includeVectorScanner = new Scanner(file);
                int numRow = 0;
                while (includeVectorScanner.hasNextLine()) {
                    String currStr = includeVectorScanner.nextLine();
                    if(numRow == currRow){
                        if(data == null){
                            data = currStr.split(",");
                        }else{
                            String[] currVector = currStr.split(",");
                            double sp = Double.valueOf(data[3]) + Double.valueOf(currVector[3]);
                            if(sp >= 20){
                                break;
                            }
                            // Пропускаем первые элементы с описанием и сп
                            for (int k = 3; k < data.length; k++) {
                                data[k] = String.valueOf(Double.valueOf(data[k]) + Double.valueOf(currVector[k]));
                            }
                        }
                        break;
                    }
                    numRow++;
                }
            }
            if (data != null) {
                StringBuilder metricsRow = new StringBuilder();
                for (String metric : data) {
                    metricsRow.append(metric).append(",");
                }
                metricsRow.append("\n");
                writer.write(metricsRow.toString());
            }
        }
    }
}
