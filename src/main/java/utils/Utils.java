package utils;

import entity.ComplexityReport;
import entity.Halstead;
import entity.Operands;
import entity.Operators;
import entity.Sloc;

public class Utils {
    public static ComplexityReport getComplexityDiff(ComplexityReport parent, ComplexityReport current) {
        if(parent == null || current == null) {
            System.out.println("Can't get complexity diff of null");
            return null;
        }
        ComplexityReport diff = new ComplexityReport();
        diff.setCyclomatic(Math.abs(parent.getCyclomatic() - current.getCyclomatic()));
        diff.setCyclomaticDensity(Math.abs(parent.getCyclomaticDensity() - current.getCyclomaticDensity()));
        diff.setParamCount(Math.abs(parent.getParamCount() - current.getParamCount()));

        Halstead diffHalstead = new Halstead();
        diffHalstead.setBugs(Math.abs(parent.getHalstead().getBugs() - current.getHalstead().getBugs()));
        diffHalstead.setDifficulty(Math.abs(parent.getHalstead().getDifficulty() - current.getHalstead().getDifficulty()));
        diffHalstead.setEffort(Math.abs(parent.getHalstead().getEffort() - current.getHalstead().getEffort()));
        diffHalstead.setLength(Math.abs(parent.getHalstead().getLength() - current.getHalstead().getLength()));
        diffHalstead.setTime(Math.abs(parent.getHalstead().getTime() - current.getHalstead().getTime()));
        diffHalstead.setVocabulary(Math.abs(parent.getHalstead().getVocabulary() - current.getHalstead().getVocabulary()));
        diffHalstead.setVolume(Math.abs(parent.getHalstead().getVolume() - current.getHalstead().getVolume()));

        Operators diffOperators = new Operators();
        diffOperators.setDistinct(Math.abs(parent.getHalstead().getOperators().getDistinct() - current.getHalstead().getOperators().getDistinct()));
        diffOperators.setTotal(Math.abs(parent.getHalstead().getOperators().getTotal() - current.getHalstead().getOperators().getTotal()));
        diffHalstead.setOperators(diffOperators);

        Operands diffOperands = new Operands();
        diffOperands.setDistinct(Math.abs(parent.getHalstead().getOperands().getDistinct() - current.getHalstead().getOperands().getDistinct()));
        diffOperands.setTotal(Math.abs(parent.getHalstead().getOperands().getTotal() - current.getHalstead().getOperands().getTotal()));
        diffHalstead.setOperands(diffOperands);

        diff.setHalstead(diffHalstead);

        Sloc diffSloc = new Sloc();
        diffSloc.setLogical(Math.abs(parent.getSloc().getLogical() - current.getSloc().getLogical()));
        diffSloc.setPhysical(Math.abs(parent.getSloc().getPhysical() - current.getSloc().getPhysical()));
        diff.setSloc(diffSloc);

        return diff;
    }
}
