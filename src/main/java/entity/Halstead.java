package entity;

public class Halstead {
    private Double bugs = 0.0;
    private Double difficulty = 0.0;
    private Double effort = 0.0;
    private Double length = 0.0;
    private Double time = 0.0;
    private Double vocabulary = 0.0;
    private Double volume = 0.0;
    private Operands operands = new Operands();
    private Operators operators = new Operators();

    public Double getBugs() {
        return bugs;
    }

    public void setBugs(Double bugs) {
        this.bugs = bugs;
    }

    public Double getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Double difficulty) {
        this.difficulty = difficulty;
    }

    public Double getEffort() {
        return effort;
    }

    public void setEffort(Double effort) {
        this.effort = effort;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Double getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Double vocabulary) {
        this.vocabulary = vocabulary;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Operands getOperands() {
        return operands;
    }

    public void setOperands(Operands operands) {
        this.operands = operands;
    }

    public Operators getOperators() {
        return operators;
    }

    public void setOperators(Operators operators) {
        this.operators = operators;
    }
}
