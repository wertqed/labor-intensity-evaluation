package entity;

public class Operands {
    private Double distinct = 0.0;
    private Double total = 0.0;

    public Double getDistinct() {
        return distinct;
    }

    public void setDistinct(Double distinct) {
        this.distinct = distinct;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
