package entity;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class ComplexityReport {
    private Double cyclomatic = 0.0;
    private Double cyclomaticDensity = 0.0;
    private Halstead halstead = new Halstead();
    private Double paramCount = 0.0;
    private Sloc sloc = new Sloc();

    public Double getCyclomatic() {
        return cyclomatic;
    }

    public void setCyclomatic(Double cyclomatic) {
        this.cyclomatic = cyclomatic;
    }

    public Double getCyclomaticDensity() {
        return cyclomaticDensity;
    }

    public void setCyclomaticDensity(Double cyclomaticDensity) {
        this.cyclomaticDensity = cyclomaticDensity;
    }

    public Halstead getHalstead() {
        return halstead;
    }

    public void setHalstead(Halstead halstead) {
        this.halstead = halstead;
    }

    public Double getParamCount() {
        return paramCount;
    }

    public void setParamCount(Double paramCount) {
        this.paramCount = paramCount;
    }

    public Sloc getSloc() {
        return sloc;
    }

    public void setSloc(Sloc sloc) {
        this.sloc = sloc;
    }

    public void addComplexity(ComplexityReport report) {
        if (report == null) {
            return;
        }
        this.setCyclomatic(this.getCyclomatic() + report.getCyclomatic());
        this.setCyclomaticDensity(this.getCyclomaticDensity() + report.getCyclomaticDensity());
        this.setParamCount(this.getParamCount() + report.getParamCount());

        this.getHalstead().setBugs(this.getHalstead().getBugs() + report.getHalstead().getBugs());
        this.getHalstead().setDifficulty(this.getHalstead().getDifficulty() + report.getHalstead().getDifficulty());
        this.getHalstead().setEffort(this.getHalstead().getEffort() + report.getHalstead().getEffort());
        this.getHalstead().setLength(this.getHalstead().getLength() + report.getHalstead().getLength());
        this.getHalstead().setTime(this.getHalstead().getTime() + report.getHalstead().getTime());
        this.getHalstead().setVocabulary(this.getHalstead().getVocabulary() + report.getHalstead().getVocabulary());
        this.getHalstead().setVolume(this.getHalstead().getVolume() + report.getHalstead().getVolume());

        this.getHalstead().getOperators().setDistinct(this.getHalstead().getOperators().getDistinct() + report.getHalstead().getOperators().getDistinct());
        this.getHalstead().getOperators().setTotal(this.getHalstead().getOperators().getTotal() + report.getHalstead().getOperators().getTotal());

        this.getHalstead().getOperands().setDistinct(this.getHalstead().getOperands().getDistinct() + report.getHalstead().getOperands().getDistinct());
        this.getHalstead().getOperands().setTotal(this.getHalstead().getOperands().getTotal() + report.getHalstead().getOperands().getTotal());

        this.getSloc().setLogical(this.getSloc().getLogical() + report.getSloc().getLogical());
        this.getSloc().setPhysical(this.getSloc().getPhysical() + report.getSloc().getPhysical());
    }

    public String toCsvString() {
        double[] metrics = new double[]{
                this.getCyclomatic(),
                this.getCyclomaticDensity(),
                this.getParamCount(),
                this.getHalstead().getBugs(),
                this.getHalstead().getDifficulty(),
                this.getHalstead().getEffort(),
                this.getHalstead().getLength(),
                this.getHalstead().getTime(),
                this.getHalstead().getVocabulary(),
                this.getHalstead().getVolume(),
                this.getHalstead().getOperators().getTotal(),
                this.getHalstead().getOperators().getDistinct(),
                this.getHalstead().getOperands().getTotal(),
                this.getHalstead().getOperands().getDistinct(),
                this.getSloc().getLogical(),
                this.getSloc().getPhysical()
        };
        return StringUtils.join(ArrayUtils.toObject(metrics), ",");
    }
}
