package entity;

public class Sloc {
    private Double logical = 0.0;
    private Double physical = 0.0;

    public Double getLogical() {
        return logical;
    }

    public void setLogical(Double logical) {
        this.logical = logical;
    }

    public Double getPhysical() {
        return physical;
    }

    public void setPhysical(Double physical) {
        this.physical = physical;
    }
}
