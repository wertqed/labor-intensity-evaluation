import com.cdancy.bitbucket.rest.BitbucketApi;
import com.cdancy.bitbucket.rest.BitbucketClient;
import com.cdancy.bitbucket.rest.domain.commit.Commit;
import com.cdancy.bitbucket.rest.domain.commit.CommitPage;
import com.cdancy.bitbucket.rest.domain.system.Version;

import java.io.FileWriter;
import java.io.IOException;

public class BitbucketFilesLoader {
    public static void main(String[] args) throws IOException {
        BitbucketClient client = BitbucketClient.builder()
                .endPoint("http://bitbucket:7990") // Optional and can be sourced from system/env. Falls back to http://127.0.0.1:7990
                .credentials("user:pass") // Optional and can be sourced from system/env and can be Base64 encoded.
                .build();

        Version version = client.api().systemApi().version();
//        client.api().commitsApi().get("PAVN", "planeta-analysis-front", "45e06e5f796297e45c9541873bbd5ec3479929f3", "")
        BitbucketApi api = client.api();
        int startIndex = 0;
        int limit = Constants.limit;
        FileWriter writer = new FileWriter("bitbucket-commit-hash.csv");
        while (true) {
            CommitPage commitPage = api.commitsApi().list("qwer", "qwer-analysis-front", null, null, null, null, null, null, null, limit, startIndex);
            for (Commit commit : commitPage.values()) {
                if (!commit.message().contains("Merge")) { //TODO пока пусть так ограничение мержей
                    System.out.println(commit.id() + " " + commit.message().replace("\n", " "));
                    writer.append(commit.id() + "," + commit.message().replace("\n", " ") + "\n");
                }
            }
            if (commitPage.isLastPage()) {
                break;
            }
            startIndex += limit;
        }

        writer.close();
    }
}
