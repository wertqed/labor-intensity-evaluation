import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CsvGenerateUtil {

    public static void main(String[] args) throws IOException {
        Scanner jiraScanner = new Scanner(new File("jira-issues-with-points.csv"));

        FileWriter writer = new FileWriter("jira+hash+clean.csv", false);

        while (jiraScanner.hasNextLine()) {
            //issueKey, issueType, Assignee, story points
            String[] jiraVal = jiraScanner.nextLine().split(",");
            String jiraKey = jiraVal[0];
            String jiraKeyNumber = "#" + jiraKey.substring(jiraKey.indexOf("-") + 1);
            Scanner bitbucketScanner = new Scanner(new File("bitbucket-commit-hash.csv"));
            List<String> hashes = new ArrayList<String>();
            while (bitbucketScanner.hasNextLine()) {
                String[] bitbucketValues = bitbucketScanner.nextLine().split(",");
                String bbMessage = bitbucketValues[1];
                // hash, message
                if ((bbMessage.contains(jiraKey) && !bbMessage.matches("(.*)" + jiraKey + "(\\d+)(.*)"))
                        || (bbMessage.contains(jiraKeyNumber) && !bbMessage.matches("(.*)" + jiraKeyNumber + "(\\d+)(.*)"))) {
                    hashes.add(bitbucketValues[0]);
                }
            }
            bitbucketScanner.close();
            if(!hashes.isEmpty()) {
                writer.append(jiraKey + "," + jiraVal[3] + "," + String.join("|", hashes) + "\n");
            }
            System.out.println(jiraKey + " " + hashes.toString());
        }

        jiraScanner.close();
        writer.close();
    }
}
