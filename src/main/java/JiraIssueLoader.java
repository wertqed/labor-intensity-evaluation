import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.SearchRestClient;
import com.atlassian.jira.rest.client.domain.*;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class JiraIssueLoader {

    private static final String JIRA_URL = "url-to-jira";
    private static final String JIRA_USERNAME = "";//логин
    private static final String JIRA_PASSWORD = "";//пароль

    public static void load() {
        // Construct the JRJC client
        JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();

        try {
            URI uri = new URI(JIRA_URL);
            JiraRestClient client = factory.createWithBasicHttpAuthentication(uri, JIRA_USERNAME, JIRA_PASSWORD);

            // Invoke the JRJC Client
            Promise<User> promise = client.getUserClient().getUser("USER");
            User user = promise.claim();

            int maxPerQuery = 100;
            int startIndex = 0;
            String jql = "project = PROJNUM";

            SearchRestClient searchRestClient = client.getSearchClient();
            String csvFile = "jira-issues-with-points.csv";
            FileWriter writer = new FileWriter(csvFile);
            Issue test = client.getIssueClient().getIssue("NUM").claim();

            while (true) {
                Promise<SearchResult> searchResult = searchRestClient.searchJql(jql, maxPerQuery, startIndex);
                SearchResult results = searchResult.claim();

                for (BasicIssue issue : results.getIssues()) {
                    Issue loadedIssue = client.getIssueClient().getIssue(issue.getKey()).claim();
                    String line  = issue.getKey() + "," + loadedIssue.getIssueType().getName() + "," +
                            (loadedIssue.getAssignee() != null ? loadedIssue.getAssignee().getDisplayName() : "")
                            + "," + ((loadedIssue.getFieldByName("Story Points") != null &&
                            loadedIssue.getFieldByName("Story Points").getValue() != null)?
                            loadedIssue.getFieldByName("Story Points").getValue().toString() : "");
                    System.out.println(line);
                    if((loadedIssue.getFieldByName("Story Points") != null &&
                            loadedIssue.getFieldByName("Story Points").getValue() != null)){
                        writer.append(line+ "\n");
                    }
                }

                if (startIndex >= results.getTotal()) {
                    break;
                }

                startIndex += maxPerQuery;

                System.out.println("Fetching from Index: " + startIndex);
            }
            writer.close();

            // Done
            System.out.println("Jira issues were successfully loaded");
        } catch (URISyntaxException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
