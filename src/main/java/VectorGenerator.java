import com.cdancy.bitbucket.rest.BitbucketApi;
import com.cdancy.bitbucket.rest.BitbucketClient;
import com.cdancy.bitbucket.rest.domain.commit.Commit;
import com.cdancy.bitbucket.rest.domain.pullrequest.Change;
import com.cdancy.bitbucket.rest.domain.pullrequest.Parents;
import entity.ComplexityReport;
import utils.Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class VectorGenerator {
    private static BitbucketClient client = BitbucketClient.builder()
            .endPoint("http://scm.fss.ibs.ru:7990") // Optional and can be sourced from system/env. Falls back to http://127.0.0.1:7990
            .credentials(":") // Optional and can be sourced from system/env and can be Base64 encoded.
            .build();
    private static BitbucketApi api = client.api();

    private static ComplexityReport getComplexity(String hash) {
        ComplexityReport resultReport = new ComplexityReport();
        Commit commit = api.commitsApi().get(Constants.project, Constants.repo, hash, null);
        List<Parents> parents = commit.parents();
        if (parents.size() > 1) {
            System.out.println("More than one parent at " + hash);
        }

        List<Change> changes = api.commitsApi().listChanges(Constants.project, Constants.repo,
                hash, Constants.limit, 0).values();
        changes = changes.stream()
                .filter(change ->
                        change.path().extension() != null &&
                                (change.path().extension().equals("ts") ||
                                        change.path().extension().equals("tsx") ||
                                        change.path().extension().equals("js") ||
                                        change.path().extension().equals("jsx")))
                .collect(Collectors.toList());

        changes.forEach(change -> {
            String parentContent = api.fileApi().raw(Constants.project, Constants.repo, change.path()._toString(), parents.get(0).id()).value();
            String currentContent = api.fileApi().raw(Constants.project, Constants.repo, change.path()._toString(), hash).value();
            ComplexityReport parentReport = ComplexityServiceApi.getComplexity(parentContent);
            ComplexityReport commitReport = ComplexityServiceApi.getComplexity(currentContent);
            if (commitReport == null || parentReport == null) {
                System.out.println("Report is null for " + change.path()._toString() + " in " + hash);
            } else {
                resultReport.addComplexity(Utils.getComplexityDiff(parentReport, commitReport));
            }
        });
        return resultReport;
    }

    public static void main(String[] args) throws IOException {
        JiraIssueLoader.load();
        Scanner jiraScanner = new Scanner(new File("jira-issues-with-points.csv"));

        FileWriter writer = new FileWriter("code_complexity_reports.csv", false);
        writer.write("Jira Key," +
                "Jira Type," +
                "Jira Assignee," +
                "Story Points," +
                "Cyclomatic," +
                "CyclomaticDensity," +
                "ParamCount," +
                "Halstead Bugs," +
                "Halstead Difficulty," +
                "Halstead Effort," +
                "Halstead Length," +
                "Halstead Time," +
                "Halstead Vocabulary," +
                "Halstead Volume," +
                "Halstead Operators Total," +
                "Halstead Operators Distinct," +
                "Halstead Operands Total," +
                "Halstead Operands Distinct," +
                "Sloc Logical," +
                "Sloc Physical" +
                "\n");

        try {
            while (jiraScanner.hasNextLine()) {
                //issueKey, issueType, Assignee, story points
                String jiraRow = jiraScanner.nextLine();
                String[] jiraVal = jiraRow.split(",");
                String jiraKey = jiraVal[0];
                String jiraKeyNumber = "#" + jiraKey.substring(jiraKey.indexOf("-") + 1);
                Scanner bitbucketScanner = new Scanner(new File("bitbucket-commit-hash.csv"));
                List<String> hashes = new ArrayList<>();
                while (bitbucketScanner.hasNextLine()) {
                    String[] bitbucketValues = bitbucketScanner.nextLine().split(",");
                    String bbMessage = bitbucketValues[1];
                    // hash, message
                    if ((bbMessage.contains(jiraKey) && !bbMessage.matches("(.*)" + jiraKey + "(\\d+)(.*)"))) {
                        hashes.add(bitbucketValues[0]);
                    }
                }
                bitbucketScanner.close();
                if (hashes.size() > 0) {
                    ComplexityReport issueReport = new ComplexityReport();
                    hashes.forEach(hash -> {
                        issueReport.addComplexity(VectorGenerator.getComplexity(hash));
                    });
                    writer.write(jiraRow + "," + issueReport.toCsvString() + "\n");
                    System.out.println(jiraKey + " " + hashes.toString());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            jiraScanner.close();
            writer.close();
        }

    }
}
